﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using MongoDB.Bson;
using Edoc.Data;
using Edoc.Interfaces;
using Edoc.Models;

namespace Edoc.Controllers
{
    [Produces("application/json")]
    [Consumes("application/json", "multipart/form-data")]
    [Route("api/[controller]")]
    public class DocumentController : Controller
    {
        private readonly IDocumentRepository _docRep;

        public DocumentController(IDocumentRepository documentRepository)
        {
            _docRep = documentRepository;
        }

        // Возвращает все документы
        [HttpGet]
        public Task<IEnumerable<Document>> Get()
        {
            return GetDocumentInterval();
        }

        private async Task<IEnumerable<Document>> GetDocumentInterval()
        {
            return await _docRep.GetAllDocuments();
        }

        // Получить конкретный документ
        [HttpGet("{id}")]
        public Task<Document> Get(string id)
        {
            return GetNoteByIdInternal(id);
        }

        private async Task<Document> GetNoteByIdInternal(string id)
        {
            return await _docRep.GetDocument(id) ?? new Document();
        }


        // POST Создает новый документ
        [HttpPost]
        public void Post([FromBody]Document doc)
        {
            _docRep.AddDocument(doc);
        }

        // PUT Обновляет документ
        [HttpPut("{id}")]
        public void Put(string id, [FromBody]Document doc)
        {
            _docRep.UpdateDocument(id, doc);
        }

        // DELETE Удаляет документ
        [HttpDelete("{id}")]
        public void Delete(string id)
        {
            _docRep.RemoveDocument(id);
        }
    }
}