﻿using System;
using Microsoft.AspNetCore.Mvc;
using Edoc.Interfaces;
using Edoc.Models;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Edoc.Controllers
{
    [Route("api/[controller]")]
    public class SystemController : Controller
    {
        private readonly IDocumentRepository _docRep;

        public SystemController(IDocumentRepository docRep)
        {
            _docRep = docRep;
        }

        // Call an initialization - api/system/init
        [HttpGet("{setting}")]
        public string Get(string setting)
        {
            if (setting == "init")
            {
                _docRep.AddDocument(new Document() { Id = "1", Body = "Test note 1", CreatedOn = DateTime.Now, UpdatedOn = DateTime.Now});
                _docRep.AddDocument(new Document() { Id = "2", Body = "Test note 2", CreatedOn = DateTime.Now, UpdatedOn = DateTime.Now});
                _docRep.AddDocument(new Document() { Id = "3", Body = "Test note 3", CreatedOn = DateTime.Now, UpdatedOn = DateTime.Now});
                _docRep.AddDocument(new Document() { Id = "4", Body = "Test note 4", CreatedOn = DateTime.Now, UpdatedOn = DateTime.Now});

                return "Done";
            }

            return "Unknown";
        }
    }
}