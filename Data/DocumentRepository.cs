﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Edoc.Interfaces;
using Edoc.Models;

namespace Edoc.Data
{
    public class DocumentRepository : IDocumentRepository
    {
        private readonly DocumentContext _context = null;

        public DocumentRepository(IOptions<Settings> settings)
        {
            _context = new DocumentContext(settings);
        }

        public async Task<IEnumerable<Document>> GetAllDocuments()
        {
            try
            {
                var r = await _context.Documents.Find(_ => true).ToListAsync();
                return r;
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task<Document> GetDocument(string id)
        {
            var filter = Builders<Document>.Filter.Eq("Id", id);

            try
            {
                return await _context.Documents
                                .Find(filter)
                                .FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task AddDocument(Document item)
        {
            try
            {
                await _context.Documents.InsertOneAsync(item);
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task<DeleteResult> RemoveDocument(string id)
        {
            try
            {
                return await _context.Documents.DeleteOneAsync(
                     Builders<Document>.Filter.Eq("Id", id));
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task<ReplaceOneResult> UpdateDocument(string id, Document doc)
        {
            try
            {
                return await _context.Documents.ReplaceOneAsync(n => n.Id.Equals(id)
                                            , doc
                                            , new UpdateOptions { IsUpsert = true });
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }


    }
}