﻿

var DocWindow = Ext.extend(Ext.Window, {

    constructor: function (config) {

        config = config || {};

        this.totalSum = new Ext.form.NumberField({
            fieldLabel: 'Общая сумма документа', readOnly: true, name: 'totalSum'
        });

        var sm = new Ext.grid.CheckboxSelectionModel({singleSelect: true});

        this.grid = new Ext.grid.EditorGridPanel({
            title: 'Детали документа',
            region: 'center',
            id: 'detailsGrid',
            width: 700,
            height: 300,
            tbar: [
                { text: 'Добавить', handler: this.detailAdd, scope: this },
                { text: 'Удалить', handler: this.detailRemove, scope: this }
            ],
            xtype: 'editorgrid',
            selModel: sm,
            loadMask: true,
            columns: [
                sm,
                { dataIndex: 'orderName', hidden: false, header: "Товарная позиция", width: 200, editable: true, editor: { xtype: 'textfield' } },
                { dataIndex: 'orderCount', hidden: false, header: "Количество", width: 150, editable: true, editor: { xtype: 'numberfield' } },
                { dataIndex: 'orderPrice', hidden: false, header: "Цена", width: 150, editable: true, editor: { xtype: 'numberfield' } },
                { dataIndex: 'orderSum', hidden: false, header: "Сумма", width: 150, editable: true, editor: { xtype: 'numberfield' } }
            ],
            store: {
                xtype: 'jsonstore',
                // store configs
                autoDestroy: true,
                url: null,
                listeners: {
                    update: { fn: this.onDetailsUpdate, scope: this},
                    remove: { fn: this.onDetailsRemove, scope: this}
                },
                storeId: 'myStore',
                // reader configs
                fields: ['orderName', { name: 'orderPrice', type: 'float' }, { name: 'orderCount', type: 'float' }, { name: 'orderSum', type: 'float' }]
            }
        });

        Ext.applyIf(config, {
            title: 'Форма документа',
            width: 800,
            height: 600,
            buttons: [
                { text: 'Сохранить', handler: this.onSaveDocumentButtonClick , scope: this}
            ],
            items: [
                {
                    padding: 10,
                    xtype: 'form',
                    items: [
                        { xtype: 'datefield', fieldLabel: 'Дата создания', readOnly: true, name: 'createdOn'},
                        { xtype: 'datefield', fieldLabel: 'Дата обновления', readOnly: true, name: 'updatedOn' },
                        { xtype: 'textfield', fieldLabel: 'Идентификатор документа', readOnly: true, name: 'id' },
                        { xtype: 'textarea', fieldLabel: 'Комментарий', width: 300, name: 'body' },
                        this.totalSum
                    ]

                },
                this.grid
            ]
        })


        DocWindow.superclass.constructor.call(this, config);

        if (!Ext.isEmpty(config.id_doc)) {
            this.loadDocData(this.id_doc);
        }
    },

    loadDocData: function(id) {
        var win = this;

        fetch('/api/document/' + id).then(function (response) {
            response.json().then(function (data) {
                console.log(data);
                data.createdOn = Date.parseDate(data.createdOn, "c");
                data.updatedOn = Date.parseDate(data.updatedOn, "c");
                win.items.get(0).getForm().setValues(data);
                win.grid.getStore().loadData(data.orders);
            });
        })
    },

    onSaveDocumentButtonClick: function () {
        var win = this;
        var doc = win.items.get(0).getForm().getValues();
        Ext.iterate(doc, function (key, value) {
            if (Ext.isEmpty(value)) {
                delete doc[key];
            }
        })
        doc.orders = [];
        win.items.get(1).getStore().each(function (record) {
            doc.orders.push(record.data);
        })


        var method = Ext.isEmpty(doc.id) ? 'POST' : 'PUT';

        var id = Ext.isEmpty(doc.id) ? '' : '/' + doc.id;

        if (!Ext.isEmpty()) {
            delete doc.id;
        }

        fetch('/api/document' + id,
            {
                method: method,
                body: JSON.stringify(doc),
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                if (response.status === 200) {
                    alert('Данные сохранены');
                    win.close();
                }
            })

    },

    detailAdd: function(){
        var grid = this.grid;
        var store = grid.getStore();
        store.add(
            [
                new store.recordType({
                    orderName: '',
                    orderCount: 0,
                    orderPrice: 0,
                    orderSum: 0
                })
            ]
        );
    },

    detailRemove: function () {
        this.grid.getStore().remove(this.grid.getSelectionModel().getSelections());
    },

    calcTotalSum: function() {
        var store = this.grid.getStore();
        var total = 0;
        store.each(function (record) {
            total += record.get('orderSum');
        });

        var win = this;
        this.totalSum.setValue(total);

    },

    onDetailsUpdate: function (store, record, operation) {
        record.set('orderSum', record.get('orderCount') * record.get('orderPrice'))
        this.calcTotalSum();
    },

    onDetailsRemove: function (store, index) {
        this.calcTotalSum();
    }


});