﻿


function reload(uuid) {

    var grid = Ext.ComponentMgr.get('documentsGrid');

    grid.getStore().reload();

}

function addDocHandler() {

    var grid = Ext.ComponentMgr.get('documentsGrid');

    new DocWindow().show();

}

function editDocHandler() {
    var grid = Ext.ComponentMgr.get('documentsGrid');
    var id = grid.getSelectionModel().getSelected().get('id');
    new DocWindow({ id_doc: id}).show();
}

function removeHandler() {
    var grid = Ext.ComponentMgr.get('documentsGrid');
    var id = grid.getSelectionModel().getSelected().get('id');
    fetch('/api/document/' + id,
        {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function (response) {
            if (response.status === 200) {
                alert('Данные удалены');
                win.grid.getStore().reload();
            }
        })
}

Ext.onReady(function () {
    new Ext.Viewport({
        layout: 'fit',
        items: {
            title: 'Диалоговый компонент',
            region: 'center',
            id: 'documentsGrid',
            width: 500,
            tbar: [
                { text: 'Добавить документ', handler: addDocHandler },
                { text: 'Редактировать документ', handler: editDocHandler },
                { text: 'Удалить документ', handler: removeHandler},
                { text: 'Загрузить документы', handler: reload }
            ],
            xtype: 'grid',
            loadMask: true,
            columns: [
                { dataIndex: 'id', hidden: true, header: "ID", width: 200},
                { dataIndex: 'createdOn', hidden: false, header: "Дата создания", xtype: 'datecolumn', format: 'd.m.Y H:m:s', width: 200},
                { dataIndex: 'updatedOn', hidden: false, header: "Дата обновления", xtype: 'datecolumn', format: 'd.m.Y H:m:s', width: 200},
                { dataIndex: 'body', hidden: false, header: "Комментарий", width: 200},
                { dataIndex: 'totalSum', hidden: false, header: "Общая сумма", width: 200}
            ],
            store: {
                xtype: 'jsonstore',
                // store configs
                autoDestroy: true,
                url: '/api/document',
                storeId: 'myStore',
                // reader configs
                fields: ['body', 'id', { name: 'totalSum', type: 'float' }, { name: 'updatedOn', type: 'date' }, { name: 'createdOn', type: 'date' }]
            }
        }

    });

});

