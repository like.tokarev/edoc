﻿using System;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace Edoc.Models
{
    public class Order
    {
        public string OrderName { get; set; }
        public decimal OrderPrice { get; set; }
        public int OrderCount { get; set; }
        public decimal OrderSum { get; set; }
    }

    public class Document
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        [BsonIgnoreIfDefault]
        public string Id { get; set; }
        public string Body { get; set; } = string.Empty;
        public DateTime UpdatedOn { get; set; } = DateTime.Now;
        public DateTime CreatedOn { get; set; } = DateTime.Now;
        public string Username { get; set; } = string.Empty;
        public decimal TotalSum { get; set; } = 0;
        public Order[] Orders { get; set; } = new Order[0];

    }

}