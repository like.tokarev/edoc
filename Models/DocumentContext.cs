﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using Edoc.Models;
using MongoDB.Driver;

namespace Edoc
{
    public class DocumentContext
    {

        private readonly IMongoDatabase _database = null;

        public DocumentContext(IOptions<Models.Settings> settings)
        {
            var client = new MongoClient(settings.Value.ConnectionString);
            if (client != null)
                _database = client.GetDatabase(settings.Value.Database);
        }

        public IMongoCollection<Document> Documents
        {
            get
            {
                var c = _database.GetCollection<Document>("documents");
                return c;
            }
        }

            
    }
}