﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Edoc.Models;
using MongoDB.Driver;
using Microsoft.AspNetCore.Http;
using MongoDB.Bson;
using System;

namespace Edoc.Interfaces
{
    public interface IDocumentRepository
    {
        Task<IEnumerable<Document>> GetAllDocuments();
        Task<Document> GetDocument(string id);
        Task AddDocument(Document item);
        Task<DeleteResult> RemoveDocument(string id);
        Task<ReplaceOneResult> UpdateDocument(string id, Document body);
    }
}